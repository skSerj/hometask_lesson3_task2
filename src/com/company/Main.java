package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        Ввести n строк с консоли. Вывести на консоль те строки, длина которых меньше средней.
        Scanner scan = new Scanner(System.in);
        System.out.println("введите количество строк, которое Вы желаете ввести: ");
        int numRows = Integer.parseInt(scan.nextLine());

        String[] arrayOFRows = new String[numRows];

        for (int i = 0; i < numRows; i++) {
            System.out.printf("Введите строку %d: ", i + 1);
            String row = scan.nextLine();
            arrayOFRows[i] = row;
        }

        int sumOFRows = 0;
        for (int i = 0; i < arrayOFRows.length; i++) {
            sumOFRows = sumOFRows + arrayOFRows[i].length();
        }
        System.out.println("сумма символов строк " + sumOFRows);

        int arithmeticMeanOfRows = sumOFRows / numRows;
        System.out.println("средняя длинна строки: " + arithmeticMeanOfRows);

        int lowerLengthRows = 0;
        String lowerLengthRowsInArray = arrayOFRows[0];

        for (int i = 0; i < numRows; i++) {
            if (arrayOFRows[i].length() < arithmeticMeanOfRows) {
                lowerLengthRows = arrayOFRows[i].length();
                lowerLengthRowsInArray = arrayOFRows[i];
                System.out.println("Строка с длинной меньше средней: " + lowerLengthRowsInArray);
            }
        }
    }
}